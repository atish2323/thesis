#https://en.wikipedia.org/wiki/Wikipedia:List_of_English_contractions
import re
from thesaurus import Word
contractions = {
"ain't": "am not / are not",
"aren't": "are not / am not",
"can't": "cannot",
"can't've": "cannot have",
"'cause": "because",
"could've": "could have",
"couldn't": "could not",
"couldn't've": "could not have",
"didn't": "did not",
"doesn't": "does not",
"don't": "do not",
"hadn't": "had not",
"hadn't've": "had not have",
"hasn't": "has not",
"haven't": "have not",
"he'd": "he had / he would",
"he'd've": "he would have",
"he'll": "he shall / he will",
"he'll've": "he shall have / he will have",
"he's": "he has / he is",
"how'd": "how did",
"how'd'y": "how do you",
"how'll": "how will",
"how's": "how has / how is",
"i'd": "I had / I would",
"i'd've": "I would have",
"i'll": "I shall / I will",
"i'll've": "I shall have / I will have",
"i'm": "I am",
"i've": "I have",
"isn't": "is not",
"it'd": "it had / it would",
"it'd've": "it would have",
"it'll": "it shall / it will",
"it'll've": "it shall have / it will have",
"it's": "it has / it is",
"let's": "let us",
"ma'am": "madam",
"mayn't": "may not",
"might've": "might have",
"mightn't": "might not",
"mightn't've": "might not have",
"must've": "must have",
"mustn't": "must not",
"mustn't've": "must not have",
"needn't": "need not",
"needn't've": "need not have",
"o'clock": "of the clock",
"oughtn't": "ought not",
"oughtn't've": "ought not have",
"shan't": "shall not",
"sha'n't": "shall not",
"shan't've": "shall not have",
"she'd": "she had / she would",
"she'd've": "she would have",
"she'll": "she shall / she will",
"she'll've": "she shall have / she will have",
"she's": "she has / she is",
"should've": "should have",
"shouldn't": "should not",
"shouldn't've": "should not have",
"so've": "so have",
"so's": "so as / so is",
"that'd": "that would / that had",
"that'd've": "that would have",
"that's": "that has / that is",
"there'd": "there had / there would",
"there'd've": "there would have",
"there's": "there has / there is",
"they'd": "they had / they would",
"they'd've": "they would have",
"they'll": "they shall / they will",
"they'll've": "they shall have / they will have",
"they're": "they are",
"they've": "they have",
"to've": "to have",
"wasn't": "was not",
"we'd": "we had / we would",
"we'd've": "we would have",
"we'll": "we will",
"we'll've": "we will have",
"we're": "we are",
"we've": "we have",
"weren't": "were not",
"what'll": "what shall / what will",
"what'll've": "what shall have / what will have",
"what're": "what are",
"what's": "what has / what is",
"what've": "what have",
"when's": "when has / when is",
"when've": "when have",
"where'd": "where did",
"where's": "where has / where is",
"where've": "where have",
"who'll": "who shall / who will",
"who'll've": "who shall have / who will have",
"who's": "who has / who is",
"who've": "who have",
"why's": "why has / why is",
"why've": "why have",		
"will've": "will have",
"won't": "will not",
"won't've": "will not have",
"would've": "would have",
"wouldn't": "would not",
"wouldn't've": "would not have",
"y'all": "you all",
"y'all'd": "you all would",
"y'all'd've": "you all would have",
"y'all're": "you all are",
"y'all've": "you all have",
"you'd": "you had / you would",
"you'd've": "you would have",
"you'll": "you shall / you will",
"you'll've": "you shall have / you will have",
"you're": "you are",
"you've": "you have"
}
vb="VB"
adjective="JJ"
def resolve_contractions(statement):
    for word in statement.split():
        if word.lower() in contractions:
            statement = statement.replace(word, contractions[word.lower()])
    return statement


home = '/home/atish'
from nltk.tag.stanford import StanfordPOSTagger as POS_Tag
_path_to_model = home + '/stanford-postagger/models/english-bidirectional-distsim.tagger'
_path_to_jar = home + '/stanford-postagger/stanford-postagger.jar'
st = POS_Tag(model_filename=_path_to_model, path_to_jar=_path_to_jar)
#return st.tag(statement.split())

def process_negation(s1,s2):
    s1_tagged=st.tag(s1.split())
    s2_tagged=st.tag(s2.split())
    similarity=0.0
    a = len(s1_tagged)
    b = len(s2_tagged)
    i=-1
    j=-1
    length = max(a, b)
    v1=[0]*length
    v2=[0]*length
    if (u'not', u'RB') or (u'no', u'RB') or (u'no', u'DT') in s1_tagged:
        try:
            i = s1_tagged.index((u'not', u'RB'))
        except:
            pass
        try:
            i=s1_tagged.index((u'no', u'RB'))
        except:
            pass
        try:
            i=s1_tagged.index((u'no', u'DT'))
        except:
            pass
        #print i
        if i!=-1:
            v1[i]=1

            if re.search(vb,str(s1_tagged[i+1])):
                v1[i+1]="VB"
            else:
                re.search(vb,str(s1_tagged[i+2]))
                v1[i+2]="VB"
                #print "found it"


            if re.search(adjective,str(s1_tagged[i+1])):
                v1[i+1]="JJ"
            elif re.search(adjective,str(s1_tagged[i+2])):
                v1[i+2]="JJ"
                #print "found it"


    if (u'not', u'RB') or (u'no', u'RB') or (u'no', u'DT') in s2_tagged:
        try:
            j = s2_tagged.index((u'not', u'RB'))
        except:
            pass
        try:
            j=s2_tagged.index((u'no', u'RB'))
        except:
            pass
        try:
            j=s2_tagged.index((u'no', u'DT'))
        except:
            pass
        #print j
        if j!=-1:
            v2[j]=1

            if re.search(vb,str(s2_tagged[j+1])):
                v2[j+1]="VB"
            else:
                try:
                    re.search(vb,str(s2_tagged[j+2]))
                    v2[j+2]="VB"
                    #print "found it"
                except:
                    pass


            if re.search(adjective,str(s2_tagged[j+1])):
                v2[j+1]="JJ"
            elif re.search(adjective,str(s2_tagged[i+2])):
                try:
                    v2[j+2]="JJ"
                    #print "found it"
                except:
                    pass
            noun="NN"
            if re.search(noun,str(s2_tagged[j+1])):
                v2[j+1]="NN"
            elif re.search(noun,str(s2_tagged[i+2])):
                try:
                    v2[j+2]="NN"
                    #print "found it"
                except:
                    pass

    print v1,v2
    # this is where we compare the two lists. I have come up with conditions to compare these lists.
    # if both of the lists contain 1(not)
    if 1 in v1 and 1 in v2:
        # what is the pos followed by not in both the li
        pos_v1=v1[v1.index(1)+1]
        pos_v2=v2[v2.index(1+1)]
        if re.search(vb,pos_v1):
        # if verb then compare verbs using wordnet
            #print "both not and verb"
            verb1=s1_tagged[v1.index(1)]
            verb2=s2_tagged[v2.index(1)]
            if verb1==verb2:
                similarity = 1.0
            if verb1!=verb2:
                #return normal similarity
                #can use thesaurus synonyms
                if(any(verb1 in x for x in Word(verb2).synonyms('all', partOfSpeech='verb'))):
                    similarity=1.0
                else:
                    pass

        if re.search(adjective,pos_v1):
        # if adjective then compare adjectives using nodebox
            #print "both not and adjectives"
            adjective1 = s1_tagged[v1.index(1)]
            adjective2 = s2_tagged[v2.index(1)]
            if adjective1==adjective2:
                similarity=1.0
            if adjective1!=adjective2:
                if (any(adjective1 in x for x in Word(adjective2).synonyms('all', partOfSpeech='adjective'))):
                    similarity = 1.0
                else:
                    pass
                #check if they are antonyms
    # if only one list contains 1(not)


    elif 1 in v1 or 1 in v2:
        regex1 = re.compile(".*(VB).*")
        regex2=re.compile(".*(JJ)*.")
        # what is the pos followed by not
        if 1 in v1:
            pos_v1 = v1[v1.index(1) + 1]
            if pos_v1==vb:
                verb1 = s1_tagged[v1.index(1)]
                verb2=[m.group(0) for l in s2_tagged for m in [regex1.search(str(l))] if m]
                if verb1 in Word(str(verb2[0].split(",")[0]).replace('(u','').replace("'","")).synonyms('all',partOfSpeech='verb'):
                    similarity=0.0
                if verb1 in Word(str(verb2[0].split(",")[0]).replace('(u','').replace("'","")).antonyms('all',partOfSpeech='verb'):
                    similarity=1.0
            if pos_v1==adjective:
                #get adjective from s2_tagged
                adjective1 = s1_tagged[v1.index(1)]
                adjective2 = [m.group(0) for l in s2_tagged for m in [regex2.search(str(l))] if m]
                if adjective1 in Word(str(adjective2[0].split(",")[0]).replace('(u','').replace("'","")).synonyms('all', partOfSpeech='adjective'):
                    similarity = 0.0
                if adjective1 in Word(str(adjective2[0].split(",")[0]).replace('(u','').replace("'","")).antonyms('all', partOfSpeech='adjective'):
                    similarity = 1.0
        if 1 in v2:
            pos_v2 = v2[v2.index(1) + 1]
            if pos_v2 == vb:
                verb2 = s2_tagged[v2.index(1)]
                verb1 = [m.group(0) for l in s1_tagged for m in [regex1.search(str(l))] if m]
                if verb2 in Word(str(verb1[0].split(",")[0]).replace('(u','').replace("'","")).synonyms('all', partOfSpeech='verb'):
                    similarity = 0.0
                if verb2 in Word(str(verb1[0].split(",")[0]).replace('(u','').replace("'","")).antonyms('all', partOfSpeech='verb'):
                    similarity = 1.0
            if pos_v2 == adjective:
                # get adjective from s2_tagged
                adjective2 = s2_tagged[v2.index(1)]
                adjective1 = [m.group(0) for l in s1_tagged for m in [regex2.search(str(l))] if m]
                if adjective2 in Word(str(adjective1[0].split(",")[0]).replace('(u','').replace("'","")).synonyms('all', partOfSpeech='adjective'):
                    similarity = 0.0
                if adjective2 in Word(str(adjective1[0].split(",")[0]).replace('(u','').replace("'","")).antonyms('all', partOfSpeech='adjective'):
                    similarity = 1.0
        # if pos is verb then compare it with verb from other sentence, similarly for adjective
    # if none of them contains 1(not)
        #print "one not and verb or adjective"
        # process as a general sentence?
    #print similarity
    return similarity
#resolved_sentence = resolve_contractions("this isn't a bad idea")
#resolved_sentence = resolve_contractions("There is no man in a black jacket doing tricks on a motorbike")
s1="There is no man in a black jacket doing tricks on a motorbike"
#print s1_tagged

#resolved_sentence = resolve_contractions("A person in a black jacket is doing tricks on a motorbike")
s2 = "A person in a black jacket is doing tricks on a motorbike"
#print s2_tagged

#process_negation(s1,s2)
