from itertools import izip_longest

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return izip_longest(*args, fillvalue=fillvalue)

with open('sentences') as f:
    for lines in grouper(f, 3, ''):
        assert len(lines) == 3
        for l in lines:
            print lines[0].rstrip()
            print lines[1].rstrip()
            print lines[2].rstrip()
            print "\n"