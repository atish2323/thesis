import time
import os
import string
#import re
start_time=time.time()
from nltk.corpus import wordnet as wn

def get_synsets(word):
    syns = wn.synsets(word)
    for syn in syns:
        #assert isinstance(syn.pos, object)
        print syn
        offset = str(syn.offset()).zfill(8) + '-' + syn.pos()
        print offset

#get_synsets('data')
def get_data_from_wordnetDomains():
    domains={}
    file = open('wordnetDomains2', 'w')
    with open('/home/atish/PycharmProjects/naturalLanguageProcessing/wn-domains-3.2/wn-domains-3.2-20070223')as f:
        count = 0
        new_count=0
        for line in f:
            #print line
            split_line=line.split("\t")
            d= split_line[1]
            #print d
            new_d=d.split(" ")
            #print new_d
            for ele in new_d:
                ele.rstrip()
                if domains.has_key(ele):
                    domains[ele] = domains[ele] + 1
                else:
                    domains[ele] = 1
    for ele, number in domains.items():
        domain_data =  ele.rstrip(),number
        #print domain_data
        file.write(str(domain_data).replace("(", "").replace(")", "").replace("'","").replace(", ",":"))
        file.write("\n")
    file.close()
#get_data_from_wordnetDomains()

def create_files_for_domains():
    with open('wordnetDomains2')as f:
        count=0
        for line in f:
            lineSplit=line.split(":")
            domain = lineSplit[0]
            count=lineSplit[1]
            tempPath="domains/"+domain+str(count)
            printable = set(string.printable)
            path=filter(lambda x: x in printable, tempPath)
            file = open(path,'w')
            file.close()
#create_files_for_domains()

def update_domain_files():
    count=0
    with open('/home/atish/PycharmProjects/naturalLanguageProcessing/wn-domains-3.2/wn-domains-3.2-20070223')as f:
        for line in f:
            split_line = line.split("\t")
            offset=split_line[0]+"\n"
            domains=split_line[1]
            for d in domains.split(" "):
                path = "domains/" + d.rstrip()
                Dfile=open(path,"a")
                Dfile.write(offset)
                Dfile.close()
#update_domain_files()
def search_domains_separate_files(domain,word):
    domain_files = os.listdir("/home/atish/PycharmProjects/naturalLanguageProcessing/domains")
    count=0
    syns = wn.synsets(word)
    for file in domain_files:
        #print file
        if domain in file.rstrip():
            #print file
            count=count+1
    #print count
            for syn in syns:
                #print "in for"
                # assert isinstance(syn.pos, object)
                offset = str(syn.offset()).zfill(8) + '-' + syn.pos()
                #print offset
                with open("/home/atish/PycharmProjects/naturalLanguageProcessing/domains/"+domain)as f:
                    print domain
                    for line in f:
                        print line
                        if offset in line:
                            print syn

#search_domains_separate_files("computer_science","algorithm")

def search_domain(domain,word):
    syns = wn.synsets(word)
    with open('wn-domains-3.2/wn-domains-3.2-20070223')as f:
        for line in f:
            for syn in syns:
                offset = str(syn.offset()).zfill(8)
                #print offset
                if offset in line and domain in line:
                    print syn
#search_domain("biology","animal")


def get_synset_by_offset(offset):
    syns = list(wn.all_synsets())
    #print syns
    offsets_list = [(s.offset(), s) for s in syns]
    offsets_dict = dict(offsets_list)
    print offsets_dict[offset]

get_synset_by_offset(00161115)

print ("%s seconds"% (time.time() - start_time))