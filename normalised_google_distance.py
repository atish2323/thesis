from bs4 import BeautifulSoup
import requests
import math
def ngd_sim(word1,word2):
    print word1,word2
    G=25270000000000
    query1="https://www.google.ca/search?q={}".format(word1)
    query2="https://www.google.ca/search?q={}".format(word2)
    combined_query='https://www.google.ca/search?q={}+{}'.format(word1,word2)

    count1 = get_search_count(query1)
    count1=count1.replace(',','')
    count1=float(count1)
    #print count1
    #print math.log(count1,2)

    count2=get_search_count(query2)
    count2=count2.replace(',','')
    count2=float(count2)
    #print count2
    #print math.log(count2,2)

    combined_count=get_search_count(combined_query)
    combined_count=combined_count.replace(',','')
    combined_count=float(combined_count)
    #print combined_count
    #print math.log(combined_count,2)

    ng = float(max([math.log(count1,2), math.log(count2,2)]) - math.log(combined_count)) / float(math.log(G,2) - min([math.log(count1,2), math.log(count2,2)]))
    print "Similarity: ",ng,"\n"
    
def get_search_count(query):
    try:
        response = requests.get(query)
    except:
        print("Google response error")
    soup = BeautifulSoup(response.text, "html.parser")
    res = soup.find('div', id="resultStats").text
    res_list = res.split(" ")
    return res_list[1]

ngd_sim("cord","string")
ngd_sim("noon","string")
ngd_sim("rooster","voyage")
ngd_sim("fruit","furnace")
ngd_sim("autograph","shore")
ngd_sim("automobile","wizard")
ngd_sim("mound","stove")
ngd_sim("grin","implement")
ngd_sim("asylum","fruit")
ngd_sim("asylum","monk")
ngd_sim("graveyard","madhouse")
ngd_sim("boy","rooster")
ngd_sim("glass","magician")
ngd_sim("cushion","jewel")
ngd_sim("monk","slave")
ngd_sim("asylum","cemetery")
ngd_sim("coast","forest")
ngd_sim("cord","string")
ngd_sim("glass","tumbler")
ngd_sim("grin","smile")
ngd_sim("serf","slave")
ngd_sim("journey","voyage")
ngd_sim("autograph","signature")
ngd_sim("coast","shore")
ngd_sim("forest","woodland")
ngd_sim("implement","tool")
ngd_sim("cock","rooster")
ngd_sim("boy","lad")
ngd_sim("cushion","pillow")
ngd_sim("cemetery","graveyard")
ngd_sim("automobile","car")
ngd_sim("gem","jewel")
ngd_sim("midday","noon")