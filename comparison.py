import numpy as np
import matplotlib.pyplot as plt

N = 20
ind = np.arange(N)  # the x locations for the groups
width = 0.14       # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

yvals = [0.005,0.01,0.01,0.0125,0.015,0.0275,0.035,0.045,0.0475,0.0975,0.9,0.9125,0.915,0.92,0.955,0.96,0.97,0.98,0.985,0.985]
rects1 = ax.bar(ind, yvals, width, color='r')

zvals = [0.0899021679,0.1107359126,0.010051669,0.2017739882,0.0742552483,0.0906955651,0.0407013844,0.0899021679,0.3000900707,0.1352531491,0.0607950554,0.9780261147,0.8157293861,0.8187171724,0.4486585394,0.2462290271,0.9985079423,0.8185286992,0.8175091596,0.9993931059]
rects2 = ax.bar(ind+width, zvals, width, color='g')

kvals = [0.0899021678922,0.110735912584,0.0100516690098,0.110735912584,0.0,0.0906955650827,0.0905825968716,0.0899021678922,0.201773988197,0.0906628971024,0.246536106486,0.80074005745,0.0,0.999983412499,0.81750915958,0.0,0.998507942332,0.81852869921,0.81750915958,0.99939310594]
rects3 = ax.bar(ind+width*2, kvals, width, color='b')

lvals = [0.0,0.0407374656389,0.0100516690098,0.201773988197,0.110396967515,0.0,0.0223685270761,0.0,0.165052252748,0.135253149057,0.0905825968716,0.978026114739,0.815729386117,0.818717172381,0.547992777484,0.246229027051,0.998507942332,0.999899655671,0.81750915958,0.99939310594]
rects4 = ax.bar(ind+width*3, lvals, width, color='c')

mvals=[0.0,0.0407374656389,0.0100516690098,0.110735912584,0.0,0.0,0.0497820725179,0.0,0.110735912584,0.0906628971024,0.80074005745,0.80074005745,0.0,0.999983412499,0.669319889871,0.0,0.998507942332,0.999899655671,0.998507942332,0.99939310594]
rects5 = ax.bar(ind+width*4, mvals, width, color='m')

ax.set_ylabel('Similarity Index')
ax.set_xticks(ind+width)
ax.set_xticklabels( ("cord smile","noon string","rooster voyage","fruit furnace","autograph shore","automobile wizard","mound stove","grin implement","asylum fruit","asylum monk","coast shore","forest woodland","implement tool","cock rooster","boy lad","cushion pillow","cemetery graveyard","automobile car","gem jewel","midday noon"),rotation=40)
ax.legend((rects1[0], rects2[0], rects3[0],rects4[0],rects5[0]),('GoldStandard', 'dis_dis', 'max_dis','dis_max','max_max'),loc="upper left")

def autolabel(rects):
    for rect in rects:
        h = rect.get_height()
        #print h
        ax.text(rect.get_x()+rect.get_width()/2, 1.05*h, '%0.4f'%h,ha='center', va='bottom',rotation=90)

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)

plt.show()