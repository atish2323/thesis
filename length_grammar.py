import spacy
import numpy
nlp=spacy.load('en')
def dependency_parser(s1,s2):
    s1 = nlp(unicode(s1,"utf-8"))
    s2 = nlp(unicode(s2,"utf-8"))
    list1=[]
    list2=[]
    dependency_variable=0
    for token in s1:
        token_properties = token,token.pos_,token.dep_
        list1.append(token_properties)
    #print list1
    #print "\n"
    for token in s2:
        token_properties = token,token.pos_,token.dep_
        list2.append(token_properties)
    #print list2

    #get length and search for the root
    #get the position of nouns and verbs from both the lists
    len1= len(list1)
    len2=len(list2)
    nounIndex1=[]
    nounIndex2=[]
    verbIndex1=[]
    verbIndex2=[]

    for ele in list1:
        if 'ROOT' in str(ele):
            root1= ele
        if 'NOUN' in str(ele):
            nounIndex1.append(list1.index(ele))
        if 'VERB' in str(ele):
            verbIndex1.append(list1.index(ele))
    #print root1,nounIndex1,verbIndex1


    for ele in list2:
        if 'ROOT' in str(ele):
            root2= ele
            #print root2
        if 'NOUN' in str(ele):
            nounIndex2.append(list2.index(ele))
        if 'VERB' in str(ele):
            verbIndex2.append(list2.index(ele))
    #print root2,nounIndex2,verbIndex2

    #analyse lists and modify the parameter
    #

    if root1!=root2:
        #print "roots are not same"
        dependency_variable=dependency_variable+1

    if nounIndex1!=nounIndex2:
        #compare lengths of nounIndex lists
        if len(nounIndex1)!=len(nounIndex2):
            dependency_variable=dependency_variable+1
        # if length is different modify parameter
        dependency_variable=dependency_variable+1

    if verbIndex1!=verbIndex2:
        #compare lengths of verbIndex lists
        if len(verbIndex1)!=len(verbIndex2):
            dependency_variable=dependency_variable+1
        #if length is different modify parameter
        dependency_variable=dependency_variable+1
    val=0.10*numpy.arctan(dependency_variable)
    #print dependency_variable
    #print "FINAL VALUE",val
    return val

#dependency_parser(unicode("the boys are playing cricket on outside","utf-8"),unicode("the man is not walking towards the building","utf-8"))
#dependency_parser("A gem is a jewel or stone that is used in jewellery. ","   A jewel is a precious stone used to decorate valuable things that you wear, such as rings or necklaces.")
