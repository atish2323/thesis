from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import random
#actual = [0.005,0.01,0.01,0.0125,0.015]
#predictions = [0.0899021679,0.1107359126,0.010051669,0.2017739882,0.0742552483]
actual = [0,0,0,0,1]
predictions = [0.0899021679,0.1107359126,0.010051669,0.2017739882,0.0742552483]
false_positive_rate, true_positive_rate, thresholds = roc_curve(actual, predictions)
roc_auc = auc(false_positive_rate, true_positive_rate)

plt.title('Receiver Operating Characteristic')
plt.plot(false_positive_rate, true_positive_rate, 'b',label='AUC = %0.4f'% roc_auc)
plt.legend(loc='lower right')
plt.plot([0,1],[0,1],'r--')
plt.xlim([-0.1,1.2])
plt.ylim([-0.1,1.2])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()