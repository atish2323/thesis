from nltk.corpus import wordnet

def return_max_frequency_synset(word,ps):
    i = 0
    old_frequency = 0
    dict = {}
    return_syn=None
    syns = wordnet.synsets(word,pos=ps)
    for s in syns:
        for l in s.lemmas():
            #print l
            new_frequency=str(l.count())
            #print "NEW",new_frequency
            if int(new_frequency)>=int(old_frequency):
                return_syn=syns[i]
                #print "The return set is:",return_syn
                #old_frequency=new_frequency
                #print "OLD=",old_frequency
        i+=1
        #return_syn = wordnet.synsets('bank')[1]
    return return_syn

#print 'Final:',return_max_frequency_synset('monk').definition()