# from nltk.corpus import PlaintextCorpusReader
from pywsd import disambiguate
from nltk import sent_tokenize

corpus_root = '/home/atish/sample.txt'
f = open('wordDef3.txt', 'a')
# wordlists=PlaintextCorpusReader(corpus_root,'sample.txt')
with open(corpus_root, 'r') as myfile:
    corpus_data = myfile.read()
# print corpus_data
text = sent_tokenize(corpus_data)
# print (text)
for sent in text:
    print "\n"
    print sent
    tags = disambiguate(sent)
    tags = [i for i in tags if i[1] is not None]
    # print tags
    for ele in tags:
        eled = str(ele[1].definition())
        s = ele[0], eled
        s1 = str(s).replace("(", "").replace(")", "").replace("'", "")
        # print (s)
        f.write(str(s1.replace(",", ":", 1)))
        f.write("\n")
f.close()
