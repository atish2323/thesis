def stanford_pos_tagger(statement):
    home = '/home/atish'
    from nltk.tag.stanford import StanfordPOSTagger as POS_Tag
    _path_to_model = home + '/stanford-postagger/models/english-bidirectional-distsim.tagger'
    _path_to_jar = home + '/stanford-postagger/stanford-postagger.jar'
    st = POS_Tag(model_filename=_path_to_model, path_to_jar=_path_to_jar)
    return st.tag(statement.split())

def bloom_lookup(v): #returns the numerical value of the domain to which the verb belongs
    #Action words for bloom taxonomy
    knowledge = ["select", "list", "name", "define", "describe", "memorize", "label", "identify", "locate", "recite","state", "recognize"]
    comprehension = ["discuss","match", "restate", "paraphrase", "rewrite", "give examples", "express", "illustrate", "explain","defend", "distinguish", "summarize", "interrelate", "interpret", "extend"]
    application = ["use","write","organize", "generalize", "dramatize", "prepare", "produce", "choose", "sketch", "apply", "solve", "draw", "show", "paint"]
    analysis = ["compare", "analyze","analyse", "classify", "point out", "distinguish", "categorize", "differentiate","subdivide", "infer", "survey", "select", "prioritize"]
    synthesis = ["compose", "originate", "hypothesize", "develop", "design", "combine", "construct", "plan", "create","invent", "organize"]
    evaluation = ["judge", "relate", "weight", "criticize", "support", "evaluate", "consider", "critique", "recommend","summarize", "appraise", "compare","predict"]
    if v in knowledge:
        return 1
    if v in comprehension:
        return 2
    if v in application:
        return 3
    if v in analysis:
        return 4
    if v in synthesis:
        return 5
    if v in evaluation:
        return 6


def bloom_verbs(list_of_tagged_tokens): #returns verbs
    list_of_verbs={}
    for token in list_of_tagged_tokens:
        #print token
        pos= str(token).split(',')[1]
        pos= pos.split("'")[1]
        #print pos
        if pos=="VB": #if it is a verb, then look it up in bloom taxonomy
            #print token[0]
            action_verb=str(token[0]).lower()
            list_of_verbs[action_verb]=bloom_lookup(action_verb)
            #bt=bloom_lookup(token[0].lower())
    #print list_of_verbs
    return list_of_verbs

def bloom_index(action_verbs_list_1,action_verbs_list_2):
    comparisons=0
    total_bloom_similarity=0
    for verb1,value1 in action_verbs_list_1.iteritems():
        for verb2,value2 in action_verbs_list_2.iteritems():
            print verb1, value1
            print verb2,value2
            distance=abs(value1-value2)
            print "Distance:",distance
            absolue_bloom_index= -0.20*distance+1.0
            print "AB",absolue_bloom_index
            total_bloom_similarity=total_bloom_similarity+absolue_bloom_index
            print total_bloom_similarity
            comparisons=comparisons+1
        print "\n"
    print "comparisons=",comparisons
    print "Bloom index=", (total_bloom_similarity / comparisons)

d1=stanford_pos_tagger("apply chemical knowledge to integrate knowledge gained in other courses and to better understand the connections between the various branche")
action_verbs_list_1=bloom_verbs(d1)
print "\n"
d2=stanford_pos_tagger("To become familiar with the structures of organic molecules, especially those found in nature or those with important biological effects.")
action_verbs_list_2=bloom_verbs(d2)
bloom_index(action_verbs_list_1,action_verbs_list_2)


# 0 0.15	1.00
# 1 0.12	0.80
# 2 0.09	0.60
# 3 0.06	0.40
# 4 0.03	0.20
# 5 0.00	0.00
# If number of action verbs in is more than 1, then add the bloom index of each comparison and then take average
