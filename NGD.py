from bs4 import BeautifulSoup

def ngd_sim(nx, ny, google_searches):
    beta = 0.67
    G = 3e13
    if nx in google_searches:
        print 'Using cached results...'
        num1 = google_searches[nx]
    else:
        for i in range(3):
            try:
                r1 = requests.get('https://www.google.ca/search?espv=2&q={}&oq={}'.format(nx, nx))
            except:
                continue
            break
        soup = BeautifulSoup(r1.text, "html.parser")
        res = soup.find('div', id="resultStats").text
        splitted = res.split(" ")
        if len(splitted) == 3:
            num = splitted[1]
        else:
            num = splitted[0]
        num1 = long(num.replace(',', ''))
        google_searches[nx] = num1
    print '{} --- {}'.format(nx, num1)

    if ny in google_searches:
        print 'Using cached results...'
        num2 = google_searches[nx]
    else:
        for i in range(3):
            try:
                r2 = requests.get('https://www.google.ca/search?espv=2&q={}&oq={}'.format(ny, ny))
            except:
                continue

        soup = BeautifulSoup(r2.text, "lxml")
        res = soup.find('div', {'id': 'resultStats'}).text
        splitted = res.split(" ")
        if len(splitted) == 3:
            num = splitted[1]
        else:
            num = splitted[0]
        num2 = long(num.replace(',', ''))
        google_searches[ny] = num2
    print '{} --- {}'.format(ny, num2)

    combined = nx + ' ' + ny

    if combined in google_searches:
        print 'Using cached results...'
        num3 = google_searches[combined]
    else:
        # Send 3 requests
        for i in range(3):
            try:
                r3 = requests.get('https://www.google.ca/search?espv=2&q={}+{}&oq={}+{}'.format(nx, ny, nx, ny))
            except:
                continue
        soup = BeautifulSoup(r3.text, "lxml")
        res = soup.find('div', {'id': 'resultStats'}).text
        splitted = res.split(" ")
        if len(splitted) == 3:
            num = splitted[1]
        else:
            num = splitted[0]
        num3 = long(num.replace(',', ''))
        google_searches[combined] = num3
    print '{} --- {}'.format(combined, num3)
    ng = float(max([math.log(num1), math.log(num2)]) - math.log(num3)) / (
    math.log(G) - min([math.log(num1), math.log(num2)]))

ngd_sim("mouse","family","google")

