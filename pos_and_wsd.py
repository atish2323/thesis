from nltk.corpus import wordnet as wn
from pywsd.lesk import simple_lesk
from pywsd import disambiguate
from pywsd.similarity import max_similarity as maxsim
from nltk.corpus import wordnet_ic
from semantic_nets_corpus_statistics import length_between_synsets
from semantic_nets_corpus_statistics import hierarchical_distance
from frequency_in_wn import return_max_frequency_synset
import nltk
import math
BETA=2
ALPHA=-0.25
THETA=0.2
def tag_tokens(s):
    token_dict={}
    tokens=nltk.word_tokenize(s)
    #print tokens
    tagged_tokens=nltk.pos_tag(tokens)
    max_freq_token=return_max_frequency_synset(tagged_tokens[0][0]) # word to get max frequency
    list_of_tagged_tokens= disambiguate(s)
    #print "TAGGED",list_of_tagged_tokens
    for tuple in list_of_tagged_tokens:
        if tuple[1]is not None:
            #print tuple[1],"=",tuple[1].definition()
            pass
        token_dict[tuple[0]]=tuple[1]
    #print token_dict,max_freq_token
    return token_dict,max_freq_token

    
    #get the best synset for each word before calling the word similarity
    #ssimple_lesk from pywsd can be used to get the appropriate sense and accordingly get the corresponding synset
    #once we have correct synset, we can compare the two synsets with word similarity algorithm
def word_similarity(word_1,word_2):
    return

#dict_1=tag_tokens("ship is on the sea coast.")
#dict_2=tag_tokens("dead fish on seashore.")
#dict_1=tag_tokens("I deposit money in the bank")
#dict_2=tag_tokens("There are plenty of dead fish on the bank of the river")
#brown_ic=wordnet_ic.ic('ic-brown.dat')
#word_tuples=[["cord","smile"],["noon","string"],["rooster","voyage"],["fruit",["furnace"]],["automobile",["wizard"]],["mound",["stove"]],["cord",["smile",0.06]],["autograph",["shore",0.11]],["asylum",["fruit",0.07]],["boy",["rooster",0.16]],["coast",["forest",0.26]],["boy",["sage",0.16]],["forest",["graveyard",0.33]],["bird",["woodland",0.12]],["hill",["woodland",0.29]],["magician",["oracle",0.20]],["oracle", ["sage",0.09]],["furnace",["stove",0.30]],["magician",["wizard",0.34]],["hill",["mound",0.15]],["cemetery",["graveyard",0.51]],["cord",["string",0.49]],["glass",["tumbler",0.28]],["grin",["smile",0.32]],["serf",["slave",0.44]],["journey",["voyage",0.41]],["autograph",["signature",0.19]],["coast",["shore",0.47]],["forest",["woodland",0.26]], ["implement",["tool",0.51]],["cock",["rooster",0.94]],["boy",["lad",0.60]],["cushion",["pillow",0.29]],["automobile",["car",0.52]],["midday",["noon",0.93]],["gem",["jewel",0.65]],["grin", ["implement"]],["asylum",["monk"]],["graveyard",["madhouse"]],["glass",["magician"]],["cushion",["jewel"]],["monk",["slave"]],["asylum",["cemetery"]],["grin",["lad"]],["shore",["woodland"]],["monk",["oracle"]],["automobile",["cushion"]],["mound",["shore"]],["lad", ["wizard"]],["food",["rooster"]],["cemetery",["woodland"]],["shore",["voyage"]],["coast",["hill"]],["furnace",["implement"]],["crane",["rooster"]],["car",["journey"]],["cemetery",["mound"]],["glass",["jewel"]],["crane",["implement"]],["brother",["lad"]],["sage",["wizard"]],["bird",["crane"]],["bird",["cock"]],["food",["fruit"]],["asylum",["madhouse"]],["noon",["string"]],["brother",["monk"]]]
#word_tuples=[["cord","smile"],["noon","string"],["rooster","voyage"],["fruit","furnace"],["autograph","shore"],["automobile","wizard"],["mound","stove"],["grin","implement"],["asylum","fruit"],["asylum","monk"],["graveyard","madhouse"],["boy","rooster"],["glass","magician"],["cushion","jewel"],["monk","slave"],["asylum","cemetery"],["coast","forest"],["grin","lad"],["shore","woodland"],["monk","oracle"],["boy","sage"],["automobile","cushion"],["mound","shore"],["lad","wizard"],["forest","graveyard"],["food","rooster"],["cemetery","woodland"],["shore","voyage"],["bird","woodland"],["coast","hill"],["furnace","implement"],["crane","rooster"],["hill","woodland"],["car","journey"],["cemetery","mound"],["glass","jewel"],["magician","oracle"],["crane","implement"],["brother","lad"],["sage","wizard"],["oracle","sage"],["bird","cock"],["bird","crane"],["food","fruit"],["brother","monk"],["asylum","madhouse"],["furnace","stove"],["magician","wizard"],["hill","mound"],["cord","string"],["glass","tumbler"],["grin","smile"],["serf","slave"],["journey","voyage"],["autograph","signature"],["coast","shore"],["forest","woodland"],["implement","tool"],["cock","rooster"],["boy","lad"],["cushion","pillow"],["cemetery","graveyard"],["automobile","car"],["gem","jewel"],["midday","noon"]]
#word_tuples=[["cord","smile"]]
#for word_pair in word_tuples:
    #print word_pair[0],word_pair[1]
    dict_1,max_freq_w1=tag_tokens(word_pair[0])
    dict_2,max_freq_w2=tag_tokens(word_pair[1])
    #print max_freq_w1.definition()
    #print max_freq_w2.definition()
    for key_1 in dict_1:
        for key_2 in dict_2:
            #print dict_1[key_1],dict_2[key_2]
            if(dict_1[key_1] is not None and dict_2[key_2] is not None):
                #print dict_1[key_1], dict_2[key_2]
                #wup=max_freq_w1.wup_similarity(max_freq_w2)
                #wup=dict_1[key_1].wup_similarity(dict_2[key_2])
                #print wup

                #path_sim=dict_1[key_1].path_similarity(dict_2[key_2])
                #print "PATH SIMILARITY",path_sim
                #cal_path_sim=(math.exp(BETA * path_sim) - math.exp(-BETA *path_sim)) / (math.exp(BETA * path_sim) + math.exp(-BETA * path_sim))
                #print "calculated PATH SIM:",cal_path_sim

                #shortest_path=dict_1[key_1].shortest_path_distance(dict_2[key_2])
                #print "SHORTEST PATH",shortest_path
                #cal_shortest_path=(math.exp(ALPHA*shortest_path))
                #print "calculated SHORTEST PATH",cal_shortest_path,"\n\n"
                #print "FROM SIMILARITY ALGORITHM",w2[1]
                #print length_between_synsets(dict_1[key_1], dict_2[key_2])
                #print length_between_synsets(max_freq_w1,max_freq_w2)
                dis_dis=length_between_synsets(dict_1[key_1],dict_2[key_2])*hierarchical_distance(dict_1[key_1],dict_2[key_2])
                #max_dis=length_between_synsets(max_freq_w1,dict_2[key_2])*hierarchical_distance(max_freq_w1,dict_2[key_2])
                #dis_max=length_between_synsets(dict_1[key_1], max_freq_w2) * hierarchical_distance(dict_1[key_1],max_freq_w2)
                max_max=length_between_synsets(max_freq_w1, max_freq_w2) * hierarchical_distance(max_freq_w1, max_freq_w2)
                avg=(dis_dis+max_max)/2.0
                if dis_dis==0.0:
                    avg=max_max
                elif max_max==0.0:
                    avg=dis_dis
                elif abs(dis_dis-max_max)>0.5:
                    avg=max(dis_dis,max_max)
                #print w1,w2[0],"Dis_Dis:",dis_dis,"\t","Max_Dis",max_dis,"Dis_Max",dis_max,"\t","Max_max",max_max,"\n"
                print avg

                #print "RES SIMILARITY",dict_1[key_1].res_similarity(dict_2[key_2],brown_ic)
                #print "LIN SIMIALRITY",dict_1[key_1].lin_similarity(dict_2[key_2],brown_ic)
                #print "JCN SIMIALRITY", dict_1[key_1].jcn_similarity(dict_2[key_2], brown_ic)
                #print "previous sm"
                #print semantic_nets_corpus_statistics.length_between_synsets(dict_1[key_1],dict_2[key_2])
                #print "Lenght between synsets:",length_between_synsets(dict_1[key_1],dict_2[key_2]),"\n\n"