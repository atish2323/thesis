from bs4 import BeautifulSoup
import requests
import math


def ngd_sim(word1, word2):
    G = 25270000000000
    query1 = 'https://www.bing.com/search?q="{}+{}"'.format(word1,word2)
    query2 = 'https://www.bing.com/search?q="{}+{}"'.format(word2,word1)
    combined_query = 'https://www.bing.com/search?q={}+{}'.format(word1, word2)
    print word1,word2
    #print query1
    count1 = get_search_count(query1)
    count1 = count1.replace(',', '')
    count1 = float(count1)
    #print count1
    # #print math.log(count1, 2)
    #
    # #print query2
    count2 = get_search_count(query2)
    count2 = count2.replace(',', '')
    count2 = float(count2)
    #print count2
    #print math.log(count2, 2)

    #print combined_query
    combined_count = get_search_count(combined_query)
    combined_count = combined_count.replace(',', '')
    combined_count = float(combined_count)
    #print combined_count
    # #print math.log(combined_count, 2)
    # c=min(count1,count2)
    # c=c*2
    ng = (count1+count2)/combined_count*100
    #print "Similarity: ", ng,"\n\n"
    print ng,"\n"


def get_search_count(query):
    try:
        response = requests.get(query)
    except:
        print("Bing response error")
    soup = BeautifulSoup(response.text, "html.parser")
    res = soup.find('div', id="b_tween").text
    #print res
    res_list = res.split(" ")
    return res_list[0]


ngd_sim("cord","smile")
ngd_sim("noon","string")
ngd_sim("rooster","voyage")
ngd_sim("fruit","furnace")
ngd_sim("autograph","shore")
ngd_sim("automobile","wizard")
ngd_sim("mound","stove")
ngd_sim("grin","implement")
ngd_sim("asylum","fruit")
ngd_sim("asylum","monk")
ngd_sim("graveyard","madhouse")
ngd_sim("boy","rooster")
ngd_sim("glass","magician")
ngd_sim("cushion","jewel")
ngd_sim("monk","slave")
ngd_sim("asylum","cemetery")
ngd_sim("coast","forest")
ngd_sim("grin","lad")
ngd_sim("shore","woodland")
ngd_sim("monk","oracle")
ngd_sim("boy","sage")
ngd_sim("automobile","cushion")
ngd_sim("mound","shore")
ngd_sim("lad","wizard")
ngd_sim("forest","graveyard")
ngd_sim("food","rooster")
ngd_sim("cemetery","woodland")
ngd_sim("shore","voyage")
ngd_sim("bird","woodland")
ngd_sim("coast","hill")
ngd_sim("furnace","implement")
ngd_sim("crane","rooster")
ngd_sim("hill","woodland")
ngd_sim("car","journey")
ngd_sim("cemetery","mound")
ngd_sim("glass","jewel")
ngd_sim("magician","oracle")
ngd_sim("crane","implement")
ngd_sim("brother","lad")
ngd_sim("sage","wizard")
ngd_sim("oracle","sage")
ngd_sim("bird","cock")
ngd_sim("bird","crane")
ngd_sim("food","fruit")
ngd_sim("brother","monk")
ngd_sim("asylum","madhouse")
ngd_sim("furnace","stove")
ngd_sim("magician","wizard")
ngd_sim("hill","mound")
ngd_sim("cord","string")
ngd_sim("glass","tumbler")
ngd_sim("grin","smile")
ngd_sim("serf","slave")
ngd_sim("journey","voyage")
ngd_sim("autograph","signature")
ngd_sim("coast","shore")
ngd_sim("forest","woodland")
ngd_sim("implement","tool")
ngd_sim("cock","rooster")
ngd_sim("boy","lad")
ngd_sim("cushion","pillow")
ngd_sim("cemetery","graveyard")
ngd_sim("automobile","car")
ngd_sim("gem","jewel")
ngd_sim("midday","noon")