home='/home/atish'
from nltk.tag.stanford import StanfordPOSTagger as POS_Tag
_path_to_model = home + '/stanford-postagger/models/english-bidirectional-distsim.tagger'
_path_to_jar = home + '/stanford-postagger/stanford-postagger.jar'
st = POS_Tag(model_filename=_path_to_model, path_to_jar=_path_to_jar)
#sents=st.raw_parse_sents("analyse the programs and design new algorithms")
#tagged_list= st.tag('Discuss the application of the scientific method to the study of human thinking,development, disorders, therapy, and social processes'.split())
#tagged_list=st.tag('Identify major health informatics applications and develop basic familiarity with healthcare IT products'.split())
tagged_list=st.tag('design new for existing algorithms'.split())
for token in tagged_list:
    pos = str(token[1]).split('.')[0]
    if pos == 'VB':
        print str(token[0])