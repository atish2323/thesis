from __future__ import division
import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus import brown
import sys
import math
import numpy as np


# T1="RAM keeps things being worked with"
# T2="The CPU uses RAM as a short-term memory storage"
# T is union of T1 and T2
# print T1.split()
# T= list(set().union(T1.split(),T2.split()))  # T is lexical semantic vector
# print T
ALPHA = 0.2
BETA = 0.45
PHI = 0.2
DELTA=0.85
word_order_threshold=0.4
brown_frequencies = dict()
N = 0

################################## Word similarity ##################################

def best_pair_synset(word_1, word_2):
    max_similairty = -1.0
    synsets_1 = wn.synsets(word_1)
    # print synsets_1
    synsets_2 = wn.synsets(word_2)
    # print synsets_2

    if len(synsets_1) == 0 or len(synsets_2) == 0:
        return None, None
    else:
        max_similairty = -1.0
        best_pair = None, None
        for synset_1 in synsets_1:
            for synset_2 in synsets_2:
                similarity = synset_1.path_similarity(synset_2)
                if similarity > max_similairty:
                    max_similairty = similarity
                    best_pair = synset_1, synset_2
        return best_pair


def length_between_synsets(synset_1, synset_2):
    set_1 = set()
    set_2 = set()
    length = sys.maxint
    if synset_1 is None or synset_2 is None:
        # print "either synset_1 or synset_2 is none"
        return length
    if synset_1 == synset_2:
        length = 0.0
        # print "both are equal"
    else:
        # print "check for common elements in set"
        for x in synset_1.lemmas():
            set_1.add(str(x.name()))
        for y in synset_2.lemmas():
            set_2.add(str(y.name()))
        if len(set_1.intersection(set_2)) > 0:
            length = 1
        else:
            length = synset_1.shortest_path_distance(synset_2)
            if length is None:
                length = 0.0
    return math.exp(-ALPHA * length)


def hierarchical_distance(synset_1, synset_2):
    distance = sys.maxint
    hypernyms_dict_1 = {}
    hypernyms_dict_2 = {}
    if synset_1 is None or synset_2 is None:
        return distance
    if synset_1 == synset_2:
        distance = max(x[1] for x in synset_1.hypernym_distances())
        #print distance
    else:
        for x in synset_1.hypernym_distances():
            hypernyms_dict_1[x[0]] = x[1]
        for y in synset_2.hypernym_distances():
            hypernyms_dict_2[y[0]] = y[1]
        #print hypernyms_dict_1
        #print hypernyms_dict_2
        least_common_subsumers = set(hypernyms_dict_1.keys()).intersection(set(hypernyms_dict_2.keys()))
        #print least_common_subsumers
        if len(least_common_subsumers) > 0:
            lcs_distances = []
            for least_common_subsumer in least_common_subsumers:
                d1 = 0
                if hypernyms_dict_1.has_key(least_common_subsumer):
                    d1 = hypernyms_dict_1[least_common_subsumer]
                    # print "d1 is:", d1
                d2 = 0
                if hypernyms_dict_2.has_key(least_common_subsumer):
                    d2 = hypernyms_dict_2[least_common_subsumer]
                    # print "d2 is:", d2
                lcs_distances.append(max([d1, d2]))
                #print "list of distances:", lcs_distances
            distance = max(lcs_distances)
            #print "max distance form list", distance
        else:
            distance = 0
            #print distance
            # print "final result"
            # print (math.exp(BETA * distance) - math.exp(-BETA * distance)) / (
            # math.exp(BETA * distance) + math.exp(-BETA * distance))
    #print "distance",distance
    #print "Subsumer value:", ((math.exp(BETA * distance) - math.exp(-BETA * distance)) / (math.exp(BETA * distance) + math.exp(-BETA * distance)))
    return (math.exp(BETA * distance) - math.exp(-BETA * distance)) / ( math.exp(BETA * distance) + math.exp(-BETA * distance))


def final_word_similarity(word_1, word_2):
    s1, s2 = best_pair_synset(word_1, word_2)
    #print "S1 is", s1
    #print "S2 is", s2
    if s1 is None or s2 is None:
        return 0.0
    return (length_between_synsets(s1, s2) * hierarchical_distance(s1, s2))
################################## Word similarity ##################################

################################## Sentence similarity ##################################
#s1, s2 = best_pair_synset('oracle', 'sage')
#print length_between_synsets(s1,s2)*hierarchical_distance(s1,s2)
#print length_between_synsets(s1,s2)
#print hierarchical_distance(s1,s2)
#
# s1, s2 = best_pair_synset('boy', 'sage')
# print length_between_synsets(s1,s2)*hierarchical_distance(s1,s2)
#
# s1, s2 = best_pair_synset('autograph', 'signature')
# print length_between_synsets(s1,s2)*hierarchical_distance(s1,s2)\
#
# s1, s2 = best_pair_synset('coast', 'shore')
# print length_between_synsets(s1,s2)*hierarchical_distance(s1,s2)
#
# s1, s2 = best_pair_synset('glass', 'tumbler')
# print (length_between_synsets(s1,s2)*hierarchical_distance(s1,s2))
#
# s1, s2 = best_pair_synset('boy', 'lad')
# print (length_between_synsets(s1,s2)*hierarchical_distance(s1,s2))
#
# s1, s2 = best_pair_synset('bird', 'woodland')
# print (length_between_synsets(s1,s2)*hierarchical_distance(s1,s2))
#
# s1, s2 = best_pair_synset('tiger', 'forest')
# print (length_between_synsets(s1,s2)*hierarchical_distance(s1,s2))
#
# s1, s2 = best_pair_synset('cushion', 'pillow')
# print (length_between_synsets(s1,s2)*hierarchical_distance(s1,s2))
#
#s1, s2 = best_pair_synset('solve', 'problem')
#print s1,s2
#print (length_between_synsets(s1,s2)*hierarchical_distance(s1,s2))

############################################################################
# semantic similarity between sentenses
############################################################################
def form_joint_word_set(T1, T2, info_content_flag):
    # type: (object, object, object) -> object
    """

    :rtype: object
    :type T1: object
    """
    T1_tokens = nltk.word_tokenize(T1)
    T2_tokens = nltk.word_tokenize(T2)
    joint_word_set = set(T1_tokens).union(T2_tokens)
    vector_1 = form_semantic_vector(T1_tokens, joint_word_set, info_content_flag)
    vector_2 = form_semantic_vector(T2_tokens, joint_word_set, info_content_flag)
    #print "STAGE 2", np.dot(vector_1, vector_2.T) / (np.linalg.norm(vector_1) * np.linalg.norm(vector_2))
    return np.dot(vector_1, vector_2.T) / (np.linalg.norm(vector_1) * np.linalg.norm(vector_2))


def form_semantic_vector(tokens, joint_word_set, info_content_flag):
    sentence_set = set(tokens)
    semantic_vector = np.zeros(len(joint_word_set))
    i = 0
    for joint_word in joint_word_set:
        if joint_word in sentence_set:
            semantic_vector[i] = 1.0
            if info_content_flag:
                semantic_vector[i] = semantic_vector[i] * math.pow(info_content(joint_word), 2)
        else:
            similar_word, max_similarity = most_similar_word(joint_word, sentence_set)
            semantic_vector[i] = PHI if max_similarity > PHI else 0.0
            if info_content_flag:
                semantic_vector[i] = semantic_vector[i] * info_content(joint_word) * info_content(similar_word)
        i = i + 1
    #print "SEMANTIC VECTOR ",semantic_vector
    return semantic_vector


def info_content(token):
    global N
    if N == 0:
        for sent in brown.sents():
            for word in sent:
                word = word.lower()
                if not brown_frequencies.has_key(word):
                    brown_frequencies[word] = 0
                brown_frequencies[word] = brown_frequencies[word] + 1
                N = N + 1
    token = token.lower()
    n = 0 if not brown_frequencies.has_key(token) else brown_frequencies[token]
    return 1.0 - (math.log(n + 1) / math.log(N + 1))


def most_similar_word(token, word_set):
    max_similarity = 0
    similar_word = ""
    for temp_word in word_set:
        #print token, temp_word
        similarity_index = final_word_similarity(token, temp_word)
        if similarity_index > max_similarity:
            max_similarity = similarity_index
            similar_word = temp_word
    return similar_word, max_similarity
################################## Sentence similarity ##################################

################################## Word order similarity ##################################
def form_word_order_vector(words, joint_words, word_index):
    word_order_vector=np.zeros(len(joint_words))
    i=0
    wordset=set(words)
    for word in joint_words:
        if word in wordset:
            word_order_vector[i]=word_index[word]
        else:
            similar_word, maximum_similarity = most_similar_word(word,wordset)
            if maximum_similarity>word_order_threshold:
                word_order_vector[i]=word_index[similar_word]
            else:
                word_order_vector[i]=0
        i=i+1
    return word_order_vector

def word_order_similarity(s1, s2):
    tokens_1=nltk.word_tokenize(s1)
    tokens_2=nltk.word_tokenize(s2)
    joint_words=list(set(tokens_1).union(tokens_2))
    #print joint_words
    word_index={x[1]:x[0] for x in enumerate(joint_words)}
    r1=form_word_order_vector(tokens_1,joint_words,word_index)
    #print r1
    r2=form_word_order_vector(tokens_2,joint_words,word_index)
    #print r2
    return 1.0- np.linalg.norm(r1-r2)/np.linalg.norm(r1+r2)
################################## Word order similarity ##################################

################################## Final similarity ##################################
def final_similarity(sentence_1,sentence_2,information_content_flag):
    return DELTA*form_joint_word_set(sentence_1,sentence_2,information_content_flag)+(1.0-DELTA)*word_order_similarity(sentence_1,sentence_2)
################################## Final similarity ##################################



# form_joint_word_set("John is very nice.", "John is very nice.", True)
# form_joint_word_set("It is a dog.", "It is a cat.", True)
# form_joint_word_set("Red alcoholic drink", "A bottle of wine", True)
# form_joint_word_set("I have a pen.", "Where is ink?", True)
# form_joint_word_set("It is a dog.", "That must be your dog", True)
# form_joint_word_set("this is not happening now.", "what can i do to prevent this and take control of the situation", True)

#print best_pair_synset('i', 'is')
#print final_similarity("I like that bachelor","I like that married man",True)
#print final_word_similarity("mound", "shore")
# print final_similarity("It is a dog","it is a dog",True)
# print final_similarity("I have a pen.", "Where is ink?", True)
# print final_similarity("Dogs are animals","they are common pets",True)

# print final_similarity("Understanding of computers Programming in the C/C++ programming languages under UNIX Algorithmic language Pseudocode Problem solving and program structure Constants, variable, data types, assignments, arithmetic expressions, input and output; objectoriented.","Design, compile and execute C++ programs to solve basic problems. Describe the concept of a variable. Describe and use C++ control structures. Describe and use functions, parameters, and return values. Perform file input and output. Solve problems requiring the use of arrays. ",True)
# print final_similarity("Understanding of computers Programming in the C/C++ programming languages .","Design, compile and execute C++ programs to solve basic problems.",True)
# print final_similarity("Abstract Background and Objective In order to facilitate clinical research across multiple institutions, data harmonization is a critical requirement.","Background Summarization is a process to select important information from a source text.Summarizing strategies are the core cognitive processes in summarization activity. Since summarization can be important ",True)
#print final_similarity("My hometown and my college town have several things in common. First, both are small rural communities. For example, my hometown, Gridlock, has a population of only about 10,000 people. Similarly, my college town, Subnormal, consists of about 11,000 local residents. This population swells to 15,000 people when the college students are attending classes. A second way in which these two towns are similar is that they are both located in rural areas. Gridlock is surrounded by many acres of farmland which is devoted mainly to growing corn and soybeans. In the same way, Subnormal lies in the center of farmland which is used to raise hogs and cattle. Thirdly, these towns are similar in that they contain college campuses. Gridlock, for example, is home to Neutron College, which is famous for its Agricultural Economics program as well as for its annual Corn-Watching Festival. Likewise, the town of Subnormal boasts the beautiful campus of Quark College, which is well known for its Agricultural Engineering department and also for its yearly Hog-Calling Contest.","My hometown and my college town are similar in several ways. First, my hometown, Gridlock, is a small town. It has a population of only about 10,000 people. Located in a rural area, Gridlock is surrounded by many acres of farmland which are devoted mainly to growing corn and soybeans. Gridlock also contains a college campus, Neutron College, which is famous for its Agricultural Economics program as well as for its annual Corn-Watching Festival. As for my college town, Subnormal, it too is small, having a population of about 11,000 local residents, which swells to 15,000 people when students from the nearby college are attending classes. Like Gridlock, Subnormal lies in the center of farmland which is used to raise hogs and cattle. Finally, Subnormal is similar to Gridlock in that it also boasts a beautiful college campus, called Quark College. This college is well known for its Agricultural Engineering department and also for its yearly Hog-Calling Contest.",True);
#print final_similarity("My hometown and my college town have several things in common. First, both are small rural communities. For example, my hometown, Gridlock, has a population of only about 10,000 people. Similarly, my college town, Subnormal, consists of about 11,000 local residents. This population swells to 15,000 people when the college students are attending classes. A second way in which these two towns are similar is that they are both located in rural areas. Gridlock is surrounded by many acres of farmland which is devoted mainly to growing corn and soybeans. In the same way, Subnormal lies in the center of farmland which is used to raise hogs and cattle. Thirdly, these towns are similar in that they contain college campuses. Gridlock, for example, is home to Neutron College, which is famous for its Agricultural Economics program as well as for its annual Corn-Watching Festival. Likewise, the town of Subnormal boasts the beautiful campus of Quark College, which is well known for its Agricultural Engineering department and also for its yearly Hog-Calling Contest."," Even though Arizona and Rhode Island are both states of the U.S., they are strikingly different in many ways. For example, the physical size of each state is different. Arizona is large, having an area of 114,000 square miles, whereas Rhode Island is only about a tenth the size, having an area of only 1,214 square miles. Another difference is in the size of the population of each state. Arizona has about four million people living in it, but Rhode Island has less than one million. The two states also differ in the kinds of natural environments that each has. For example, Arizona is a very dry state, consisting of large desert areas that do not receive much rainfall every year. However, Rhode Island is located in a temperate zone and receives an average of 44 inches of rain per year. In addition, while Arizona is a landlocked state and thus has no seashore, Rhode Island lies on the Atlantic Ocean and does have a significant coastline.",True)
#print final_similarity("Understanding of computers Programming in the C C++ programming languages under UNIX Algorithmic language Pseudocode Problem solving and program structure Constants, variable, data types, assignments, arithmetic expressions, input and output; object oriented and top down design and procedures, selection and loops; functions; enumerated data type; arrays, structures, and searching and sorting.","Design, compile and execute C++ programs to solve basic problems. Describe the concept of a variable. Describe and use C++ control structures. Describe and use functions, parameters, and return values. Perform file input and output. Solve problems requiring the use of arrays. ",True)
#print final_similarity("\"She should be ashamed of herself for playing politics with this important issue,\" said state budget division spokesman Andrew Rush.","\"Senator Clinton should be ashamed of herself for playing politics with the important issue of homeland security funding,\" he said",True)
#print final_similarity("Design, compile and execute C++ programs to solve basic problems","Design and implement programs using C++",True)
#print final_similarity("A cemetery is a place where dead peoples bodies or their ashes are buried.","A graveyard is an area of land, sometimes near a church, where dead people are baried",True)